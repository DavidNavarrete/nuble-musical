<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $fillable = ['icon_id', 'slug', 'link', 'nameProfile', 'user_id'];
    public $timestamps = false;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function icon()
    {
        return $this->belongsTo('App\Icon');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
