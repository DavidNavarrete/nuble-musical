<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSocialNetworkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'icon_id' => 'required',
            'nameProfile' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'icon_id.required' => 'Selecciona una red social',
            'nameProfile.required' => 'Ingrese el nombre del perfil de su página o número teléfonico según red social'
        ];
    }
}
