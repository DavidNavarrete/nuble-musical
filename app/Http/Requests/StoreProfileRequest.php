<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'biography' => 'required',
            'music_id' => 'required',
            'avatar' => 'required|image|max:5120'
        ];
    }
    public function messages()
    {
        return [
            'biography.required' => 'Ingrese su biografía',
            'music_id.required' => 'Seleccione el tipo de música que compone',
            'avatar.required' => 'Seleccione una imagen',
            'avatar.image' => 'El avatar debe ser una imagen',
            'avatar.max' => 'El avatar no debe superar los 5 MB'
        ];
    }
}
