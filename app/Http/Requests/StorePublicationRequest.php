<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:publications,title',
            'description' => 'required',
            'image' => 'image|required|max:5120',
            'link' => 'required',
            'iframe' => 'nullable',
            'date' => 'date|nullable',
            'time' => 'nullable',
            'reason_id' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'El titulo es obligatorio',
            'title.unique' => 'El título de esta publicación ya existe',
            'description.required' => 'La descripción es obligatoria',
            'image.required' => 'Seleccione una imagen',
            'image.image' => 'El archivo debe ser una imagen',
            'image.max' => 'La imagen supera los 5 MB',
            'link.required' => 'Ingrese el link',
            'reason_id.required' => 'Seleccione el motivo de la publicación',
            'date.date' => 'No es válido como fecha'
        ];
    }
}
