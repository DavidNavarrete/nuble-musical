<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSocialNetworkRequest;
use App\Http\Requests\UpdateSocialNetworkRequest;
use Illuminate\Support\Str;
use App\SocialNetwork;
use App\Icon;
use Auth;
use App\Profile;


class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        $query = trim($request->get('searchSocialNetwork'));
        if($query){
            $socialNetworks = SocialNetwork::with('icon')->where('user_id', '=', Auth::User()->id)->whereHas('icon', function($query2) use ($request){
                $query2->where('name', 'LIKE', '%'.trim($request->get('searchSocialNetwork')).'%');
            })->orWhere('nameProfile', 'LIKE', '%'.$query.'%')->orderBy('nameProfile')->paginate(8);
            return view('crudSocialNetworks.index', compact('socialNetworks', 'query', 'profileUser'));
        }else{
            $socialNetworks = SocialNetwork::with('icon')->where('user_id', '=', Auth::User()->id)->orderBy('nameProfile')->paginate(8);
            return view('crudSocialNetworks.index', compact('socialNetworks', 'profileUser'));  
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all()->sortBy('name');
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudSocialNetworks.create', compact('icons', 'profileUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSocialNetworkRequest $request)
    {
        $socialNetwork = new SocialNetwork();
        $socialNetwork->icon_id = trim($request->get('icon_id'));
        $socialNetwork->slug = Str::slug(trim($socialNetwork->icon->name.'/'.Auth::User()->stageName));
        $socialNetwork->link = trim($request->get('link'));
        $socialNetwork->nameProfile = trim($request->get('nameProfile'));
        $socialNetwork->user_id = Auth::User()->id;
        $socialNetwork->save();
        return redirect()->route('redesSociales')->with('status', 'Red social agregada exitosamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialNetwork $redesSociale)
    {
        $socialNetwork = $redesSociale;
        $icons = Icon::all()->sortBy('name');
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudSocialNetworks.edit', compact('socialNetwork', 'icons', 'profileUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSocialNetworkRequest $request, SocialNetwork $redesSociale)
    {
        $socialNetwork = $redesSociale;
        $socialNetwork->slug = Str::slug(trim($socialNetwork->icon->name.'/'.Auth::User()->stageName));
        $socialNetwork->user_id = Auth::User()->id;
        $socialNetwork->fill($request->all());
        $socialNetwork->save();
        return redirect()->route('redesSociales')->with('status', 'Red social editada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialNetwork $redesSociale)
    {
        $socialNetwork = $redesSociale;
        $socialNetwork->delete();
        return redirect()->route('redesSociales')->with('status', 'Red social eliminada exitosamente');
    }
}
