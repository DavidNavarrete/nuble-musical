<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;

class PrincipalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('search'));
        $listUsers = User::with('profile')->get();
        if ($query) {
            $users = User::with('profile','city')->whereHas('profile', function($query2) use ($query){
                $query2->whereHas('music', function($query3) use ($query){
                    $query3->where('type', 'LIKE', '%'.$query.'%');
                });
                })->orWhereHas('city', function($query4) use ($query){
                    $query4->where('name', 'LIKE', '%'.$query.'%');
                })->orWhere('stageName', 'LIKE', '%'.$query.'%')->paginate(8);
                return view('results', compact('users', 'query', 'listUsers'));
        } else {
            $users = User::with('publications', 'profile')->paginate(6);
            return view('principal', compact('users', 'listUsers'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
