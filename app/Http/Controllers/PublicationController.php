<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePublicationRequest;
use App\Http\Requests\UpdatePublicationRequest;
use Illuminate\Support\Str;
use Auth;
use File;
use App\Publication;
use App\Reason;
use App\Profile;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        if($request){
            $query = trim($request->get('searchPublication'));
            $publications = Publication::with('user')->where('user_id', '=', Auth::User()->id)->paginate(6);
            return view('crudPublications.index', compact('publications','query', 'profileUser'));
        }else{
            $publications = Publication::with('reasons')->where('user_id', '=', Auth::User()->id)->paginate(6);
            return view('crudPublications.index', compact('publications', 'profileUser'));
        } 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reasons = Reason::all();
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudPublications.create', compact('reasons', 'profileUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePublicationRequest $request)
    {
        $publication = new Publication();
        $publication->title = trim($request->get('title'));
        $publication->slug = Str::slug(trim($request->get('title')));
        $publication->description = trim($request->get('description'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            $nameImg = time().$file->getClientOriginalName();
            $publication->image = $nameImg;
        }
        $publication->reason_id = trim($request->get('reason_id'));
        $publication->user_id = Auth::User()->id;
        $publication->link = trim($request->get('link'));
        if($request->get('iframe')  != ""){
            $publication->iframe = trim($request->get('iframe'));
        }
        if($request->get('date') && $request->get('time') != ""){
        $publication->date = trim($request->get('date'));
        $publication->time = trim($request->get('time'));
        }
        if($publication->save()){
            $file->move(public_path().'/images/publicaciones/', $nameImg);
        }
        return redirect()->route('publicaciones')->with('status', 'Publicación creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publicacione)
    {
        $publication = $publicacione;
        $reasons = Reason::all();
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudPublications.edit', compact('publication','reasons', 'profileUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePublicationRequest $request, Publication $publicacione)
    {
        $publication = $publicacione;
        if($request->hasFile('image')){
            $filePath = public_path().'/images/publicaciones/'.$publication->image;
            File::delete($filePath);
            $file = $request->file('image');
            $nameImg = time().$file->getClientOriginalName();
            $publication->image = $nameImg;
        }
        $publication->slug = Str::slug(trim($request->get('title')));
        $publication->reason_id = trim($request->get('reason_id'));
        $publication->user_id = Auth::User()->id;
        $publication->fill($request->except('image'));
        if($publication->save()){
            if(isset($file) && $file != ""){
                $file->move(public_path().'/images/publicaciones/', $nameImg);
            }
        }
        return redirect()->route('publicaciones')->with('status', 'Publicación editada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $publicacione)
    {
        $publication = $publicacione;
        $file_path = public_path() . '/images/publicaciones/' . $publication->image;
        if($publication->delete()){
            File::delete($file_path);
        }
        return redirect()->route('publicaciones')->with('status', 'Publicación eliminada exitosamente');
    }
}
