<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Music;
use App\Profile;
use App\User;
use Auth;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all()->where('user_id', '=', Auth::User()->id);
        $user = User::with('socialNetworks')->where('id', '=', Auth::User()->id)->get();
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudProfile.index', compact('profile', 'profileUser', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $music = Music::all()->sortBy('type');
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudProfile.create', compact('music', 'profileUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        $profile = new Profile();
        $profile->biography = trim($request->get('biography'));
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $nameImg = time().$file->getClientOriginalName();
            $profile->avatar = $nameImg;
        }
        $profile->user_id = Auth::User()->id;
        $profile->music_id = trim($request->get('music_id'));
        if($profile->save()){
            $file->move(public_path().'/images/avatars/', $nameImg);
        }
        return redirect()->route('perfil')->with('status', 'Datos de perfil creados exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $perfil)
    {
        $profile = $perfil;
        $music = Music::all()->sortBy('type');
        $profileUser = Profile::where('user_id', '=', Auth::User()->id)->get();
        return view('crudProfile.edit', compact('profile','music', 'profileUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, Profile $perfil)
    {
        $profile = $perfil;
        if($request->hasFile('avatar')){
            $filePath = public_path().'/images/avatars/'.$profile->avatar;
            File::delete($filePath);
            $file = $request->file('avatar');
            $nameImg = time().$file->getClientOriginalName();
            $profile->image = $nameImg;
        }
        $profile->user_id = Auth::User()->id;
        $profile->music_id = trim($request->get('music_id'));
        $profile->fill($request->except('avatar'));
        if($profile->save()){
            if(isset($file) && $file != ""){
                $file->move(public_path().'/images/avatars/', $nameImg);
            }
        }
        return redirect()->route('perfil')->with('status', 'Perfil editado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $perfil)
    {
        //
    }
}
