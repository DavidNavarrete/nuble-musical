<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['biography', 'avatar', 'user_id', 'music_id'];
    public $timestamps = false;
    public function music()
    {
        return $this->belongsTo('App\Music');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
