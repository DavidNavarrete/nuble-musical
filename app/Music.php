<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = ['type'];
    public $timestamps = false;

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
}
