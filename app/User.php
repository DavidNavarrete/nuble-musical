<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'stageName', 'city_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getRouteKeyName()
    {
        return 'stageName';
    }
    public function getStageName()
    {
        return $this->stageName;
    }
    public function getFullName()
    {
        return $this->name.' '.$this->lastname;
    }
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    public function publications()
    {
        return $this->hasMany('App\Publication');
    }
    public function profile()
    {
        return $this->hasMany('App\Profile');
    }
    public function socialNetworks()
    {
        return $this->hasMany('App\SocialNetwork');
    }
}
