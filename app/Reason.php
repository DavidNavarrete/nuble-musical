<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    protected $fillable = ['type'];
    public $timestamps = false;

    public function publications()
    {
        return $this->hasMany('App\Publication');
    }
}
