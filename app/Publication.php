<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $fillable = ['title', 'slug', 'description', 'image', 'link', 'iframe', 'date', 'time', 'user_id', 'reason_id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function reason()
    {
        return $this->belongsTo('App\Reason');
    }
}
