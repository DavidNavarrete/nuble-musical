<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    protected $fillable = ['name', 'icon'];
    public $timestamps = false;

    public function social()
    {
        return $this->hasOne('App\SocialNetwork');
    }
}
