$("#btnSubmit").closest('form').on('submit', function(e) {
    e.preventDefault();
    $('#btnSubmit').attr('disabled', true);
    $("#btnSubmit").text("Guardando...");
    this.submit();
});
$("#form").keydown(function (e){
    var keyCode= e.which;
    if (keyCode == 13){
      event.preventDefault();
      return false;
    }
  });