<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PrincipalController@index')->name('principal');
Route::get('/perfil/{user}', 'UserProfileController@index')->name('perfilUsuario');

Auth::routes();

Route::group(['middleware'=>['auth']], function(){
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('home/redesSociales', 'SocialController');
Route::get('home/redesSociales', 'SocialController@index')->name('redesSociales');
Route::resource('home/publicaciones', 'PublicationController');
Route::get('home/publicaciones', 'PublicationController@index')->name('publicaciones');
Route::resource('home/perfil', 'ProfileController');
Route::get('home/perfil', 'ProfileController@index')->name('perfil');
});
