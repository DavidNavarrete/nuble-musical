<?php

use Illuminate\Database\Seeder;
use App\Music;

class MusicSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $music = new Music();
        $music->type = "Rap";
        $music->save();
        $music = new Music();
        $music->type = "Trap";
        $music->save();
        $music = new Music();
        $music->type = "Rock";
        $music->save();
        $music = new Music();
        $music->type = "Jazz";
        $music->save();
        $music = new Music();
        $music->type = "Pop";
        $music->save();
        $music = new Music();
        $music->type = "Metal";
        $music->save();
        $music = new Music();
        $music->type = "Electrónica";
        $music->save();
        $music = new Music();
        $music->type = "Reggae";
        $music->save();
        $music = new Music();
        $music->type = "Reggaeton";
        $music->save();
        $music = new Music();
        $music->type = "Cumbia";
        $music->save();
    }
}
