<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new City();
        $city->name = 'Bulnes';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Chillán';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Chillán Viejo';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Cobquecura';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Coelemu';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Coihueco';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'El Carmen';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Ninhue';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Ñiquén';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Pemuco';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Pinto';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Portezuelo';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Quillón';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Quirihue';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Ranquil';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'San Carlos';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'San Fabián';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'San Ignacio';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'San Nicolás';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Treguaco';
        $city->region = 'Ñuble';
        $city->save();
        $city = new City();
        $city->name = 'Yungay';
        $city->region = 'Ñuble';
        $city->save();
    }
}
