<?php

use Illuminate\Database\Seeder;
use App\Reason;

class ReasonsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reason = new Reason();
        $reason->type = "Evento";
        $reason->save();
        $reason = new Reason();
        $reason->type = "Información";
        $reason->save();
        $reason = new Reason();
        $reason->type = "Nueva música";
        $reason->save();
        $reason = new Reason();
        $reason->type = "Nuevo vídeo";
        $reason->save();
        $reason = new Reason();
        $reason->type = "Otro";
        $reason->save();
    }
}
