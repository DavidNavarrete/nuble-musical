@extends('layouts.app')
@section('content')
<br>
   <section class="artista bg-white py-3">
       <div class="container-fluid my-2">
        <div class="row m-0">
            @if (isset($userFind) && $userFind ?? '')
            @foreach ($userFind as $user)
                @foreach ($user->profile as $profile)
                <div class="card col-12 shadow-lg my-2 px-0 bg-transparent">
                    <div class="row no-gutters">
                      <div class="col-12 col-md-3">
                      <img src="/images/avatars/{{$profile->avatar}}" class="card-img-top img-fluid" alt="Mi avatar" title="Mi avatar">
                      </div>
                      <div class="col-12 col-md-9">
                        <div class="card-body text-dark">
                        <h3 class="card-title font-weight-bold">{{$user->stageName}}</h3>
                          <h5 class="card-text font-weight-bold">{{$profile->music->type}} <i class="fas fa-music text-primary ml-1"></i></h5>
                          <h5 class="card-text font-weight-bold">{{$user->city->name.'/'.$user->city->region}} <i class="fas fa-city ml-1"></i></h5>
                          <address>{{$user->email}} <i class="fas fa-envelope text-primary ml-2"></i></address>
                          <blockquote class="blockquote">
                              <p class="card-text">{{$profile->biography}}</p>
                          </blockquote>
                        </div>
                      </div>
                    </div>
                  </div>
                  @if ($user->socialNetworks->count() != 0)    
                    <div class="col-12 bg-transparent shadow py-3">
                        @foreach ($user->socialNetworks as $socialNetwork)
                            <a href="{{$socialNetwork->link}}"> <i class="{{$socialNetwork->icon->icon}} fa-3x text-primary"></i> </a>
                        @endforeach
                    </div>
                  @endif
                @endforeach
            @endforeach
            @endif
        </div>
    </div>
   </section>
   <section class="menus my-3 bg-transparent shadow">
       <div class="container-fluid">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
              <a class="nav-link font-weight-bold active" id="pills-events-tab" data-toggle="pill" href="#pills-events" role="tab" aria-controls="pills-events" aria-selected="true">Eventos</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link font-weight-bold" id="pills-new-music-tab" data-toggle="pill" href="#pills-new-music" role="tab" aria-controls="pills-new-music" aria-selected="false">Nueva música</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link font-weight-bold" id="pills-new-video-tab" data-toggle="pill" href="#pills-new-video" role="tab" aria-controls="pills-new-video" aria-selected="false">Nuevos videos</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link font-weight-bold" id="pills-information-tab" data-toggle="pill" href="#pills-information" role="tab" aria-controls="pills-information" aria-selected="false">Información</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link font-weight-bold" id="pills-other-tab" data-toggle="pill" href="#pills-other" role="tab" aria-controls="pills-other" aria-selected="false">Otros</a>
            </li>
          </ul>
          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade py-4 show active" id="pills-events" role="tabpanel" aria-labelledby="pills-events-tab">
                @if (isset($userFind) && $userFind ?? '')
                    @foreach ($userFind as $user)
                    <div class="row m-0">
                        @foreach ($user->publications as $publication)
                                    @if ($publication->reason->type == "Evento")
                                        <div class="col-4">
                                        <div class="card text-center shadow-lg h-100 my-2">
                                            <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                                <div class="card-body text-center text-dark">
                                                    <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                                    <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                                    <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                                </div>
                                            </div>
                                        </div>                                
                                    @endif
                               @endforeach 
                           </div>
                    @endforeach
                @endif
            </div>
            <div class="tab-pane fade py-4" id="pills-new-music" role="tabpanel" aria-labelledby="pills-new-music-tab">
                @if (isset($userFind) && $userFind ?? '')
                @foreach ($userFind as $user)
                <div class="row m-0">
                    @foreach ($user->publications as $publication)
                                @if ($publication->reason->type == "Nueva música")
                                    <div class="col-4">
                                    <div class="card text-center shadow-lg h-100 my-2">
                                        <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                            <div class="card-body text-center text-dark">
                                                <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                                <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                                <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                           @endforeach 
                       </div>
                @endforeach
            @endif
            </div>
            <div class="tab-pane fade py-4" id="pills-new-video" role="tabpanel" aria-labelledby="pills-new-video-tab">
                    @if (isset($userFind) && $userFind ?? '')
                    @foreach ($userFind as $user)
                    <div class="row m-0">
                        @foreach ($user->publications as $publication)
                                    @if ($publication->reason->type == "Nuevo vídeo")
                                        <div class="col-4">
                                        <div class="card text-center shadow-lg h-100 my-2">
                                            <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                                <div class="card-body text-center text-dark">
                                                    <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                                    <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                                    <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                            @endforeach 
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="tab-pane fade py-4" id="pills-information" role="tabpanel" aria-labelledby="pills-information-tab">
                    @if (isset($userFind) && $userFind ?? '')
                    @foreach ($userFind as $user)
                    <div class="row m-0">
                        @foreach ($user->publications as $publication)
                                    @if ($publication->reason->type == "Información")
                                        <div class="col-4">
                                        <div class="card text-center shadow-lg h-100 my-2">
                                            <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                                <div class="card-body text-center text-dark">
                                                    <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                                    <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                                    <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                            @endforeach 
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="tab-pane fade py-4" id="pills-other" role="tabpanel" aria-labelledby="pills-other-tab">
                    @if (isset($userFind) && $userFind ?? '')
                    @foreach ($userFind as $user)
                    <div class="row m-0">
                        @foreach ($user->publications as $publication)
                                    @if ($publication->reason->type == "Otros")
                                        <div class="col-4">
                                        <div class="card text-center shadow-lg h-100 my-2">
                                            <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                                <div class="card-body text-center text-dark">
                                                    <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                                    <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                                    <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                            @endforeach 
                        </div>
                    @endforeach
                @endif
            </div>
          </div>
       </div>
   </section>
@endsection