@extends('layouts.app')

@section('content')
<div class="row m-0">
    <div class="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
        <div class="container-fluid my-4">
            <div class="row justify-content-center">
                <div class="card col-12 col-md-8 shadow-lg bg-transparent">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-left">
                    <h3 class="font-weight-bold text-uppercase my-4 text-white text-center">Regístrate</h3>
                        <form class="my-1" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label class="font-weight-bold text-white" for="name">Nombre</label>
                                        <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}" placeholder="Nombre" required>
                                        @error('name')
                                            <div class="alert alert-light p-1 my-1" role="alert"> 
                                                <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label class="font-weight-bold text-white my-1" for="lastname">Apellido</label>
                                        <input id="lastname" name="lastname" type="text" class="form-control" value="{{ old('lastname') }}" placeholder="Apellido" required>
                                        @error('lastname')
                                            <div class="alert alert-light p-1 my-1" role="alert"> 
                                                <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <label class="font-weight-bold text-white my-1" for="stageName">Nombre artístico</label>
                                <input id="stageName" name="stageName" type="text" class="form-control" value="{{ old('stageName') }}" placeholder="Nombre artístico" required>
                                    @error('stageName')
                                    <div class="alert alert-light p-1 my-1" role="alert"> 
                                        <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                    </div>
                                    @enderror
                                <label class="font-weight-bold text-white my-1" for="city">Ciudad</label>
                                <select aria-label="Ciudad" id="validationCity" class="custom-select" name="city_id" required>
                                    <option disabled selected value="">-- Seleccione una ciudad --</option>
                                    @if (isset($cities) && $cities ?? '')
                                      @foreach ($cities as $c)
                                          <option  value="{{$c->id}}" {{old('city_id') == $c->id ? 'selected': ''}}>{{$c->name}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                                  @error('city_id')
                                  <div class="alert alert-light p-1 my-1" role="alert"> 
                                    <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                  </div>
                                 @enderror
                                <label class="font-weight-bold text-white my-1" for="email">Correo electrónico</label>
                                <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}" placeholder="Correo electrónico" required>
                                @error('email')
                                   <div class="alert alert-light p-1 my-1" role="alert"> 
                                                <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                            </div>
                                @enderror
                                <label class="font-weight-bold text-white my-1" for="password">Contraseña</label>
                                <input id="password" name="password" type="password" class="form-control border" placeholder="Contraseña" required>
                                @error('password')
                                   <div class="alert alert-light p-1 my-1" role="alert"> 
                                                <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                            </div>
                                @enderror
                                        <label class="font-weight-bold text-white my-1" for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar contraseña</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"placeholder="Confirme su contraseña" autocomplete="new-password">
                                        <div class="row my-4">
                                            <div class="col-6">
                                                <button type="submit" class="btn btn-primary text-uppercase">
                                                   Registrarme <i class="fa fa-user-plus ml-1"></i>
                                                </button>
                                            </div>
                                            <div class="col-6 text-right mx-auto d-flex justify-content-end">
                                            <p class="text-center text-white h6 font-italic">¿Ya tienes una cuenta? <a href="{{route('login')}}" class="text-center text-uppercase text-white font-weight-bold d-block">Inicia sesión</a></p>
                                            </div>
                                               <div class="col-12 text-left my-2">
                                                    <a class="text-white text-uppercase" href="{{ route('principal') }}">Inicio <i class="fa fa-home ml-1"></i></a>
                                            </div>
                                        </div>
                              </div>
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
