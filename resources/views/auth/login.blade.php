@extends('layouts.app')

@section('content')
<div class="row m-0 fondo">
    <div class="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
        <div class="container-fluid my-5">
            <div class="row justify-content-center">
                <div class="card col-12 col-sm-8 shadow-lg bg-transparent">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-5 text-left">
                    <h3 class="font-weight-bold text-uppercase text-white text-center">Iniciar sesión</h3>
                        <form class="my-5" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="font-weight-bold text-white" for="email">Correo electrónico</label>
                                <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}" placeholder="Correo electrónico" required>
                                @error('email')
                                    <div class="alert alert-light p-1 my-1" role="alert"> 
                                          <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                    </div>
                                @enderror
                                <label class="font-weight-bold text-white my-2" for="password">Contraseña</label>
                                <input id="password" name="password" type="password" class="form-control border" placeholder="Contraseña" required>
                                   @error('password')
                                       <div class="alert alert-light p-1 my-1" role="alert"> 
                                          <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                    </div>
                                    @enderror
                              <div class="form-check my-3 ml-1">
                                   <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                   <label class="form-check-label text-white" for="remember">Recordar contraseña</label>
                               </div>
                                        <div class="row p-0">
                                            <div class="col-6">
                                                <button type="submit" class="btn btn-primary text-uppercase">
                                                   Iniciar sesión <i class="fa fa-user ml-1"></i>
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                @if (Route::has('password.request'))
                                                    <a class="font-weight-bold text-uppercase text-white" href="{{ route('password.request') }}">
                                                        ¿Olvidaste tu contraseña?
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-12 text-left my-3">
                                                <p class="text-white font-italic h6">¿Aún no tienes una cuenta?<a type="button" role="button" class="text-uppercase font-weight-bold text-white ml-2" href="{{route('register')}}">Regístrate</a></p>
                                            </div>
                                            <div class="col-12 text-left my-2">
                                                    <a class="text-white text-uppercase" href="{{ route('principal') }}">Inicio <i class="fa fa-home ml-1"></i></a>
                                            </div>
                                        </div>
                              </div>
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
