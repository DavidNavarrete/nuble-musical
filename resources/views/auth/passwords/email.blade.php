@extends('layouts.app')
@section('content')
    <div class="row m-0">
        <div class="col-12 col-md-12 col-lg-12 col-xl-12 p-0">
            <div class="container-fluid my-5">
                <div class="row justify-content-center">
                    <div class="card col-12 col-sm-8 shadow-lg bg-transparent">
                    <div class="col-12 col-md-12 col-xl-12 my-5 text-left">
                        @if (session('status'))
                        <div class="alert alert-light p-1 my-1" role="alert"> 
                            <span class="text-success font-weight-bold d-block">{{session('status')}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                        </div>
                        @endif
                        <h3 class="font-weight-bold text-uppercase text-white text-center">Enlace de recuperación</h3>
                            <form class="my-5" method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group">
                                    <label class="font-weight-bold text-white" for="email">Correo electrónico</label>
                                    <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}" placeholder="Correo electrónico" required>
                                    @error('email')
                                    <div class="alert alert-light p-1 my-1" role="alert"> 
                                        <span class="text-danger font-weight-bold d-block">{{$message}} <i class="fas fa-exclamation-triangle ml-1 text-danger"></i></span>
                                    </div>
                                    @enderror
                                            <div class="row my-3">
                                                <div class="col-12 text-left">
                                                    <button type="submit" class="btn btn-primary text-uppercase">
                                                       Enviar enlace <i class="fas fa-share ml-1"></i>
                                                    </button>
                                                </div>
                                                <div class="col-12 my-2 m-0">
                                                <a href="{{route('login')}}" class="text-white font-weight-bold text-uppercase text-left">Volver al inicio de sesión</a>
                                                </div>
                                            </div>
                                  </div>
                            </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
