@if (session('status'))
    <div class="alert alert-success font-weight-bold text-dark text-center">
        {{ session('status') }}
    </div>
@endif