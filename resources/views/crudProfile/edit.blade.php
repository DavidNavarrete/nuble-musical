@extends('layouts.templateHome')

@section('content')
{{-- @include('common.errors') --}}
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
            <form id="form" class="form-group" method="POST" action="/home/perfil/{{$profile->id}}" enctype="multipart/form-data">
                @method('PUT')
                    @csrf
                      <div class="card shadow-lg p-0">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Actualización de perfil</h3>
                              </div>
                          </div>
                          <div class="card-body p-0">
                            <div class="form-group col-12 col-lg-11 mx-auto">
                                <label class="d-block text-dark my-2" for="reason">Biografía</label>
                                <div class="input-group mb-2 my-1 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-pen-square text-white"></i></div>
                                    </div>
                                <textarea aria-label="Biografía" type="text" id="validationBiography" name="biography" class="form-control" placeholder="Escriba su biografía" required>{{$profile->biography}}</textarea>
                                </div>
                                @error('biography')
                                    <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                @enderror
                                <label class="d-block text-dark my-1" for="music_id">Tipo de música</label>
                                    <div class="input-group mb-2 my-1 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"> <i class="fa fa-hand-point-up text-white"></i> </div>
                                        </div>
                                        <select aria-label="Motivo" id="validationMusic" class="custom-select" name="music_id" required>
                                          <option disabled selected value="">-- Seleccione el tipo de música que hace --</option>
                                          @if (isset($music) && $music ?? '')
                                            @foreach ($music as $m)
                                        <option  value="{{$m->id}}" @if(isset($profile->music_id) && $m->id == $profile->music_id) selected @endif>{{$m->type}}</option>
                                            @endforeach
                                            </select>  
                                          @endif
                                    </div>
                                    @error('music_id')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                     @enderror
                                <label class="d-block text-dark" for="Avatar">Avatar</label>
                                <div class="custom-file">
                                <input aria-label="Avatar" id="validationAvatar" type="file" accept="image/*" name="avatar" class="custom-file-input">
                                  <label class="custom-file-label" for="avatar">Pinchame</label>
                                  <small class="text-primary">El avatar no debe superar los <b>5 MB</b></small>
                                </div> 
                                @error('avatar')
                                    <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                @enderror
                                <div class="mx-auto my-4">
                                    <a href="{{route('perfil')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                </div>
                            </div>  
                            </div>    
                          </div>
                      </div>
                  </form>
              </div>
@endsection

