@extends('layouts.templateHome')

@section('content')
          <div class="card shadow-lg mb-4 p-0">
            <div class="card-header bg-primary py-3">
              <h4 class="m-0 font-weight-bold text-white">Panel de administración de perfil</h4>
            </div>
            <div class="card-body">
              <div class="container-fluid p-0">
              @include('common.success')
              @if (Auth::User()->profile->count() == 0)
                <div class="text-right my-1 p-0">
                    <a href="{{route('perfil.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
                </div> 
              @endif
              <div class="row my-2">
                  @if (isset($profile) && $profile ?? '')
                    @foreach ($profile as $p)
                    <div class="card col-12 p-0">
                      <div class="row no-gutters">
                        <div class="col-12 col-md-4">
                        <img src="/images/avatars/{{$p->avatar}}" class="card-img-top img-fluid" alt="Mi avatar" title="Mi avatar">
                        </div>
                        <div class="col-12 col-md-8">
                          <div class="card-body text-dark">
                            <h3 class="card-title font-weight-bold font-italic">{{Auth::User()->getStageName()}} - {{Auth::User()->getFullName()}}</h3>
                            <h5 class="card-text font-weight-bold font-italic">{{$p->music->type}} <i class="fas fa-music text-primary ml-1"></i></h5>
                            <h5 class="card-text font-weight-bold font-italic">{{Auth::User()->city->name.'/'.Auth::User()->city->region}} <i class="fas fa-city ml-1"></i></h5>
                            <address>{{Auth::User()->email}} <i class="fas fa-envelope text-primary ml-2"></i></address>
                            <p class="card-text">{{$p->biography}}</p>
                            <div class="socials">
                              @foreach ($user as $u)
                                  @foreach ($u->socialNetworks as $socialNetwork)
                                    @if ($socialNetwork->link ?? '') 
                                      <a href="{{$socialNetwork->link}}" target="_blank" rel="noopener" title="{{$socialNetwork->nameProfile}}"><i class="{{$socialNetwork->icon->icon}} fa-3x text-primary"></i></a>
                                    @else
                                      <i class="{{$socialNetwork->icon->icon}} fa-3x text-primary ml-1" title="{{$socialNetwork->nameProfile}}"></i>
                                    @endif
                                  @endforeach
                              @endforeach
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="d-flex justify-content-end my-1">
                               <a href="{{route('perfil.edit', $p->id)}}" role="button" type="button" class="btn btn-primary text-white">Editar <span class="fa fa-edit"></span></a>
                     </div>
                    </div>
                    @endforeach
                  @endif
              </div>
            </div>
          </div>
        </div>     
@endsection


