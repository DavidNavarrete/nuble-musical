@extends('layouts.app')

@section('content')
<br>
<section class="search bg-white py-1">
        <div class="container mt-5 h-100">
            <h4 class="text-center text-uppercase font-weight-bold">Busca un artista</h4>
            <h6 class="text-center font-italic">Puedes buscar por nombre, ciudad o género musical</h6>
            <div class="d-flex justify-content-center h-100">
                <form class="text-white" method="get">
                    @csrf
                    <div class="searchbar shadow">
                        <input class="search_input" type="text" name="search">
                        <a type="submit" class="search_icon"><i class="fas fa-search fa-lg"></i></a>
                    </div>
                </form>
            </div>
          </div>
    </section>
@if (isset($users) && $users ?? '')
<section class="eventos bg-white">
        <h1 class="text-center text-uppercase font-weight-bold text-dark mt-5">PRÓXIMOS EVENTOS</h1>
        <div class="row d-flex justify-content-center my-2 m-0">
                @foreach ($users as $user)
                    @foreach ($user->publications as $publication)
                    @foreach ($user->profile as $profile) 
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-2">
                            <div class="card text-center shadow h-100">
                            <img src="/images/publicaciones/{{$publication->image}}" height="300" width="260" alt="{{$publication->image}}" class="card-img-top">
                                <div class="card-body text-center text-dark">
                                    <h5 class="card-title font-weight-bold text-uppercase text-truncate">{{$publication->title}}</h5>
                                    <h6 class="card-subtitle font-weight-bold text-uppercase">{{$publication->description}}</h6>
                                    <h5 class="badge badge-primary text-uppercase font-weight-bold py-2 card-subtitle my-1">{{$publication->reason->type}}</h5>
                                </div>
                                <div class="card-footer bg-transparent m-0">
                                    <div class="row">
                                        <div class="col-3 text-left">
                                        <a href="{{route('perfilUsuario', $user->stageName)}}"><img src="/images/avatars/{{$profile->avatar}}" height="60" width="60" title="Perfil de {{$user->stageName}}" alt="Perfil de {{$user->stageName}}" class="rounded-circle ml-auto shadow-lg"></a>
                                        </div>
                                        <div class="col-9">
                                            <h5 class="text-left text-uppercase font-weight-bold">{{$user->stageName}} <i class="fa fa-user-circle text-grey-50 ml-1" aria-label="Usuario"></i> </h5>
                                            @if ($publication->date && $publication->time ?? '')
                                                <p class="text-muted">Disponible el: <b>{{$publication->date}}</b> a las <b>{{$publication->time}}</b></p>
                                            @endif
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endforeach
                @endforeach
            </div>
        </section>
    @endif
@if (isset($users) && $users ?? '')
<section class="artistas bg-blue">
    <br>
    <div class="container my-4">
        <h1 class="text-center text-uppercase font-weight-bold text-white">ARTISTAS</h1>
        <div class="row d-flex justify-content-center my-2 m-0">
                @foreach ($users as $user) 
                    @foreach ($user->profile as $profile)
                        <div class="col-lg-4">
                            <div class="card bg-transparent border-0 text-center h-100">
                            <img src="/images/avatars/{{$profile->avatar}}" width="140" height="140" title="Perfil de {{$user->stageName}}" alt="Perfil de {{$user->stageName}}"class="rounded-circle mx-auto">
                                <div class="card-body text-center text-white">
                                    <h5 class="card-title font-weight-bold text-uppercase">{{$user->stageName}}</h5>
                                    <h6 class="card-title font-weight-bold text-uppercase">{{$user->name.' '.$user->lastname}}</h6>
                                    <div class="card-text font-italic">{{$profile->biography}}</div>
                                    <div class="text-center my-3">
                                    <a class="btn btn-outline-primary shadow-lg text-white" href="{{route('perfilUsuario', $user->stageName)}}">Ver perfil <i class="fa fa-user-circle ml-1" aria-label="Usuario"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>
    </section>
@endif
@if (isset($users) && $users ?? '')
<section class="videos my-4">
    <div class="container-fluid">
        <h1 class="text-center text-uppercase font-weight-bold">VÍDEOS DE ARTISTAS</h1>
        <div class="row">
            @foreach ($users as $user)
                @foreach ($user->publications as $publication)
                    @foreach ($user->profile as $profile)
                        <div class="col-12 col-lg-4 my-2">
                            <div class="embed-responsive embed-responsive-4by3">
                            <iframe loading="lazy" class="embed-responsive-item rounded" src="{{$publication->iframe}}"></iframe>
                            </div>
                            <div class="card-footer bg-transparent m-0">
                                        <div class="row">
                                            <div class="col-4 text-left">
                                                <a href="{{route('perfilUsuario', $user->stageName)}}"><img src="/images/avatars/{{$profile->avatar}}" height="90" width="90" title="Perfil de {{$user->stageName}}" alt="Perfil de {{$user->stageName}}" class="rounded-circle mx-auto shadow-lg"></a>
                                            </div>
                                            <div class="col-8">
                                                <h5 class="text-left text-uppercase font-weight-bold">{{$user->stageName}} <i class="fa fa-user-circle text-grey-50 ml-1" aria-label="Usuario"></i> </h5>
                                                @if ($publication->link ?? '')
                                                    <a href="{{$publication->link}}" class="btn btn-danger">Ir a Youtube <i class="fab fa-youtube ml-1" aria-label="Youtube"></i> </a>
                                                @endif
                                            </div>
                                        </div>   
                                    </div>
                                <hr>
                        </div>
                    @endforeach
                @endforeach
            @endforeach
        </div>
    </div>
</section>
@endif
@if (isset($users) && $users ?? '')
  <div class="d-flex justify-content-center">
        {{$users->links()}}
  </div>
@endif
@endsection