@extends('layouts.templateHome')

@section('content')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
          @if (isset($publication) && $publication ?? '') 
            <form id="form" class="form-group" method="POST" action="/home/publicaciones/{{$publication->slug}}" enctype="multipart/form-data">
                @method('PUT')
                        @csrf
                          <div class="card shadow-lg">
                              <div class="card-header bg-primary p-0">
                                  <div class="text-white text-center py-2">
                                      <h3 class="font-weight-bold"> Actualización de publicación</h3>
                                  </div>
                              </div>
                              <div class="card-body p-0">
                                <div class="form-group col-12 col-lg-11 mx-auto">
                                    <label class="d-block text-dark my-2" for="title">Título</label>
                                    <div class="input-group mb-2 my-1 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-heading text-white"></i></div>
                                        </div>
                                    <input aria-label="Título" type="text" id="validationTitle" name="title" class="form-control" value="{{$publication->title}}" placeholder="Ingrese el título de su publicación" required>
                                    </div>
                                    @error('title')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                     @enderror
                                     <label class="d-block text-dark my-1" for="description">Descripción</label>
                                    <div class="input-group mb-2 my-1 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-pen-square text-white"></i></div>
                                        </div>
                                    <textarea aria-label="Descripción" type="text" id="validationDescription" name="description" class="form-control" placeholder="Ingrese la descripción de su publicación" required>{{$publication->description}}</textarea>
                                    </div>
                                    @error('description')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                     @enderror
                                     <label class="d-block text-dark my-1" for="reason">Motivo</label>
                                    <div class="input-group mb-2 my-1 mx-auto">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"> <i class="fa fa-hand-point-up text-white"></i> </div>
                                        </div>
                                        <select aria-label="Motivo" id="validationReason" class="custom-select" name="reason_id" required>
                                          <option disabled selected value="">-- Escoga el motivo de su publicación --</option>
                                          @if (isset($reasons) && $reasons ?? '')
                                            @foreach ($reasons as $reason)
                                        <option  value="{{$reason->id}}" @if(isset($publication->reason_id) && $reason->id == $publication->reason_id) selected @endif>{{$reason->type}}</option>
                                            @endforeach
                                            </select>  
                                          @endif
                                    </div>
                                    @error('reason_id')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                     @enderror
                                    <label class="d-block text-dark" for="Image">Imagen</label>
                                    <div class="custom-file">
                                      <input aria-label="Imagen" id="validationImage" type="file" accept="image/*" name="image" class="custom-file-input">
                                      <label class="custom-file-label" for="Image">Pinchame</label>
                                      <small class="text-primary">La imagen no debe superar los <b>5 MB</b></small>
                                    </div>
                                    @error('image')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                     @enderror
                                     <div class="row">
                                         <div class="col">
                                            <label class="d-block text-dark my-1" for="link">Enlace</label>
                                            <div class="input-group mb-2 my-1 mx-auto">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-link text-white"></i></div>
                                                </div>
                                            <input aria-label="Enlace" type="url" id="validationLink" name="link" class="form-control" value="{{$publication->link}}" placeholder="Ingrese el link o enlace (Incluya https)">
                                            </div>
                                            @error('link')
                                                <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                            @enderror
                                         </div>
                                         <div class="col">
                                            <label class="d-block text-dark my-1" for="link">Iframe</label>
                                            <div class="input-group mb-2 my-1 mx-auto">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-video text-white"></i></div>
                                                </div>
                                            <input aria-label="Iframe" type="text" id="validationIframe" name="iframe" class="form-control" value="{{$publication->iframe}}" placeholder="Ingrese el iframe que ofrece Youtube, SundCloud, etc.">
                                            </div>
                                             <small class="text-primary">El campo <b>'Iframe'</b> puede quedar vacío.</small>
                                         </div>
                                     </div>
                                <div class="row">
                                    <div class="col">
                                        <label class="d-block text-dark" for="date">Fecha en base al motivo</label>
                                        <div class="input-group mx-auto">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-calendar text-white"></i></div>
                                            </div>
                                        <input aria-label="Fecha" type="date" id="validationDate" name="date" class="form-control" value="{{$publication->date}}" placeholder="Ingrese la fecha en base al motivo">
                                        </div>
                                        <small class="text-primary">El campo <b>'Fecha'</b> puede quedar vacío.</small>
                                        @error('date')
                                            <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label class="d-block text-dark" for="time">Hora en base al motivo</label>
                                        <div class="input-group mx-auto">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-calendar text-white"></i></div>
                                            </div>
                                        <input aria-label="Hora" type="time" id="validationTime" name="time" class="form-control" value="{{$publication->time}}" placeholder="Ingrese la hora en base al motivo">
                                        </div>
                                        <small class="text-primary">El campo <b>'Hora'</b> puede quedar vacío.</small>
                                        @error('time')
                                            <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                        @enderror
                                    </div>
                                </div>
                                    <div class="mx-auto my-4">
                                        <a href="{{route('publicaciones')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                        <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                    </div>
                              </div>
                          </div>
                      </form>
                @endif
              </div>
        </div>
@endsection


