@extends('layouts.templateHome')

@section('content')
          <div class="card shadow-lg mb-4 p-0">
            <div class="card-header bg-primary py-3">
              <h4 class="m-0 font-weight-bold text-white">Panel de administración de publicaciones</h4>
            </div>
            <div class="card-body">
              <div class="container-fluid p-0">
              @include('common.success')
                    <div class="text-right my-1 p-0">
                        <a href="{{route('publicaciones.create')}}" role="button" type="button" class="btn btn-primary text-white">Agregar <span class="fa fa-plus-circle"></span></a>
                    </div>
              <div class="row my-2">
                  @if (isset($publications) && $publications ?? '')
                    @foreach ($publications as $publication)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 my-2">
                        <div class="card mb-3 shadow-sm h-100">
                        <img height="350px" width="200px" src="/images/publicaciones/{{$publication->image}}" class="card-img-top" alt="Imagen de publicación" title="Imagen de {{$publication->title}}">
                            <div class="card-body text-dark">
                                <h4 class="card-title font-weight-bold text-uppercase">{{$publication->title}}</h4>
                                <h5 class="badge badge-primary card-title text-uppercase font-weight-bold py-2 mx-auto">{{$publication->reason->type}}</h5>
                                <p class="card-subtitle">{{$publication->description}}</p>
                                @if ($publication->date ?? '' && $publication->time)
                                  <p class="card-subtitle text-dark my-1">{{$publication->date}} a las {{Str::limit($publication->time, $limit="5", $end=".")}}</p>
                                @else
                                  <p class="card-subtitle text-dark my-1">Sin fecha por el momento</p>
                                @endif
                                <a href="{{$publication->link}}" target="_blank" rel="noopener" class="text-primary">Ir al enlace</a>
                                <p class="card-subtitle text-left my-1"><small class="text-dark">Creado el: <b>{{$publication->created_at}}</b></small></p>
                                <p class="card-subtitle text-left my-1"><small class="text-dark">Editado el: <b>{{$publication->updated_at}}</b></small></p>
                                <div class="text-left">
                                  <a aria-label="Editar" href="{{route('publicaciones.edit', $publication->slug)}}" role="button" type="button" class="btn btn-info text-white"><span class="fa fa-edit"></span></a>    
                                  <a aria-label="Eliminar" data-toggle="modal" data-target="#openModal{{$publication->id}}" role="button" type="button" class="btn btn-danger"> <span class="fa fa-trash text-white"></span></a>
                                </div>
                                <div class="modal fade" id="openModal{{$publication->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header bg-primary text-white">
                                        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Eliminar publicación</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span class="text-white active" aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body font-weight-bold">
                                          <div class="text-center">
                                            <i class="fa fa-exclamation-triangle fa-2x text-primary"></i>
                                          </div>
                                          ¿Está seguro de eliminar esta publicación: "{{$publication->title}}" ?
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                          <form method="POST" action="{{route('publicaciones.destroy', $publication)}}">
                                            @method('DELETE')
                                            @csrf
                                              <button type="submit" class="btn btn-primary">Sí, eliminar <span class="fa fa-trash ml-2"></span></button>
                                            </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            </div>
                      </div>
                    @endforeach
                  @endif
              </div>
              @if (isset($publications) && $publications ?? '')
                  <div class="d-flex justify-content-center">
                    {{$publications->links()}}
                  </div>
              @endif
            </div>
          </div>
        </div>      
@endsection


