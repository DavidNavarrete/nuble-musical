<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="/images/logotipo.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Panel de Administración Ñuble Musical">
    <meta name="author" content="Ñuble Musical">
    <!-- CSRF Token -->
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- Icons --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>
    @if (Request::path() == 'login' || Request::path() == 'register' || Request::path() == 'password/reset')
    <style>
        #nav{
            display: none;
        }
        body{
            background: url('/images/wallpapers/loginWallpaper.jpg') repeat-y !important;
            background-size: cover !important;
        }
        footer{
            display: none;
        }
        #logoCopy{
            display: block !important;
        }
    </style>
    @endif
    <div id="app">
        <nav id="nav" class="navbar fixed-top navbar-expand-md navbar-light bg-blue shadow-sm mb-2">
            <div class="container-fluid">
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img class="img-fluid" src="/images/logotipo.ico" alt="Logo" title="Logo"> Ñuble Musical
                </a>
                <button class="navbar-toggler btn btn-outline-primary bg-transparent shadow-lg" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="fa fa-bars fa-lg text-white py-2"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link text-uppercase text-white" href="{{ route('principal') }}">Inicio <i class="fa fa-home ml-1"></i></a>
                        </li>
                        @if (isset($listUsers) && $listUsers ?? '')
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle text-white text-uppercase" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artistas <i class="fas fa-music ml-1"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item disabled font-weight-bold" aria-disabled="true">Región de Ñuble</a>
                                <div class="dropdown-divider"></div>
                                @if ($user->profile ?? '' && $user->profile->count() < 0)
                                    @foreach ($listUsers as $user)    
                                            @foreach ($user->profile as $profile)
                                                <a aria-label="Submenu" class="dropdown-item py-2 px-2" href="{{route('perfilUsuario', $user->stageName)}}"> <img src="/images/avatars/{{$profile->avatar}}" height="48" width="48" alt="Perfil de {{$user->stageName}}" title="Perfil de {{$user->stageName}}" class="rounded-circle d-inline ml-0 mr-2"> {{$user->stageName}}</a>
                                            @endforeach
                                        @endforeach
                                @endif
                            </div>
                            </li>
                        @endif
                            <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="{{ route('login') }}">Iniciar sesión <i class="fa fa-user ml-1"></i></a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase text-white" href="{{ route('register') }}">Registrarme <i class="fa fa-user-plus ml-1"></i></a>
                                </li>
                            @endif
                            
                        @else
                        <li class="nav-item">
                            <a class="nav-link text-uppercase text-white" href="{{ route('principal') }}">Inicio <i class="fa fa-home ml-1"></i></a>
                        </li>
                            @if (isset($listUsers) && $listUsers ?? '')
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle text-white text-uppercase" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Artistas <i class="fas fa-music ml-1"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item disabled font-weight-bold" aria-disabled="true">Región de Ñuble</a>
                                    <div class="dropdown-divider"></div>
                                    @if ($user->profile ?? '' && $user->profile->count() < 0)
                                        @foreach ($listUsers as $user)    
                                                @foreach ($user->profile as $profile)
                                                    <a aria-label="Submenu" class="dropdown-item py-2 px-2" href="{{route('perfilUsuario', $user->stageName)}}"> <img src="/images/avatars/{{$profile->avatar}}" height="48" width="48" alt="Perfil de {{$user->stageName}}" title="Perfil de {{$user->stageName}}" class="rounded-circle d-inline ml-0 mr-2"> {{$user->stageName}}</a>
                                                @endforeach
                                            @endforeach
                                    @endif
                                </div>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link text-white dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::User()->getStageName() }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesión <i class="fa fa-sign-out-alt"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <br>
        @yield('content')
    </div>
    <footer class="bg-blue">
        <div class="row m-0">
            <div class="col-12 text-center">
                <h3 class="text-white my-3">Gracias por visitar nuestro sitio web</h3>
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img class="img-fluid" src="/images/logotipo.ico" alt="Logo" title="Logo"> Ñuble Musical
                </a>
                <small class="d-block mb-3 text-white">&copy; Todos los derechos reservados - {{date('Y')}}</small>
            </div>
        </div>
    </footer>
    <footer class="bg-transparent"  id="logoCopy">
        <div class="row m-0 d-flex justify-content-end">
            <div class="col-12 text-right">
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img class="img-fluid" src="/images/logotipo.ico" alt="Logo" title="Logo"> Ñuble Musical
                </a>
                <small class="d-block mb-3 text-white">&copy; Todos los derechos reservados - {{date('Y')}}</small>
            </div>
        </div>
    </footer>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
