
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="/images/logotipo.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Panel de Administración Ñuble Musical">
  <meta name="author" content="Ñuble Musical">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Panel De Administración</title>
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
</head>

<body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center text-white">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-music"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Ñuble Musical</div>
      </a>
      <hr class="sidebar-divider">
      <div class="sidebar-heading mr-auto">
        Administración
      </div>
      <li class="nav-item active">
      <a class="nav-link" href="{{route('perfil')}}">
          <i class="fas fa-fw fa-user"></i>
          <span>Mi perfil</span></a>
      </li>
      <li class="nav-item active">
      <a class="nav-link" href="{{route('redesSociales')}}">
          <i class="fas fa-fw fa-link"></i>
          <span>Mis redes sociales</span></a>
      </li>
      <li class="nav-item active">
      <a class="nav-link" href="{{route('publicaciones')}}">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Mis publicaciones</span></a>
        </li>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <button aria-label="Menu" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars fa-2x"></i>
          </button>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-inline text-dark small">@if (Auth::check()) {{Auth::User()->getStageName()}} @endif</span>
               @if (isset($profileUser))
                   @foreach ($profileUser as $avatarUser)
                   @if (Auth::check())
                    <img class="img-profile rounded-circle" src="/images/avatars/{{$avatarUser->avatar}}" alt="Mi avatar" title="Mi avatar">
                    @else
                    <img class="img-profile rounded-circle" src="/images/avatars/avatar-default.png" alt="Avatar" title="Avatar">
                    @endif
                    @endforeach
               @endif
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-dark"></i>
                  Cerrar sesión
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <div class="container-fluid">
            @yield('content')
        </div>
      </div>
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span class="text-dark"><b>Copyright &copy; Ñuble Musical - {{date('Y')}}</b> </span>
          </div>
        </div>
      </footer>
    </div>
  </div>
   <a aria-label="Subir" class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up bg-´gradient-primary"></i>
  </a>
  {{-- Modal Cerrar Sesión --}}
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="exampleModalLabel">¿Estás seguro(a)?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span class="text-white" aria-hidden="false">×</span>
          </button>
        </div>
        <div class="modal-body text-dark">Seleccione "Sí, cerrar sesión" para cerrar la sesión</div>
        <div class="modal-footer">
          <button class="btn btn-light" type="button" data-dismiss="modal">Cancelar</button>
       <a class="btn btn-primary" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Sí, cerrar sesión') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
        </form>
        </div>
      </div>
    </div>
  </div>

   <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
   <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
   <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
   <script src="{{asset('js/sb-admin-2.min.js')}}"></script>
   <script src="{{asset('js/rulesForms.js')}}"></script>
</body>
</html>
