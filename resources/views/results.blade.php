@extends('layouts.app')
@section('content')
<br>
<section class="search bg-white py-1 my-5">
        <div class="container mt-5 h-100">
            <h4 class="text-center text-uppercase font-weight-bold">Busca un artista</h4>
            <h6 class="text-center font-italic">Puedes buscar por nombre, ciudad o género musical</h6>
            <div class="d-flex justify-content-center h-100">
                <form class="text-white" method="get">
                    @csrf
                    <div class="searchbar shadow">
                        <input class="search_input" type="text" name="search">
                        <a type="submit" class="search_icon"><i class="fas fa-search fa-lg"></i></a>
                    </div>
                </form>
            </div>
          </div>
    </section>
    <div class="container-fluid my-1 py-y">
        @if(isset($query) && $query ?? '') 
              @if (isset($users) && $users->count() > 0)
              <div class="alert alert-success text-left">
                <div class="blockquote my-1">
                    Los resultados para <h6 class="font-weight-bold d-inline">"{{$query}}"</h6> son:  
                </div>
              </div>
              @else
              <div class="alert alert-success">
                    <div class="text-center">
                            <i class="fas fa-frown fa-6x d-block text-primary"></i>
                            <div class="blockquote">
                                <p class="font-weight-bold d-block text-uppercase">Lo sentimos</p>
                                <p>No existen resultados para: <b class="d-inline">"{{$query}}"</b></p>
                            </div> 
                        </div>
                </div>
                  <div class="d-flex justify-content-center">
                        <a href="{{route('principal')}}" type="submit" role="button" class="btn btn-outline-primary bg-transparent">Volver al inicio</a>
                    </div>
              @endif
        @endif
     </div>
     @if (isset($users) && $users ?? '')
        <section class="artist my-3">
            <div class="row m-0">
                @foreach ($users as $user)
                    @foreach ($user->profile as $profile)          
                        <div class="col-3 my-2">
                            <div class="card shadow h-100 my-3">
                            <img src="/images/avatars/{{$profile->avatar}}" height="250" width="250" class="card-img-top" title="Perfil de {{$user->stageName}}" alt="Perfil de {{$user->stageName}}">
                                <div class="card-body">
                                <h5 class="card-title text-uppercase text-center">{{$user->stageName}}</h5>
                                <h6 class="card-subtitle text-center">{{$user->city->name}} / {{$user->city->region}}</h6>
                                <h6 class="card-title text-center">{{$profile->music->type}}</h6>
                                <p class="card-text text-center">{{$profile->biography}}</p>
                                <div class="text-center">
                                     <a href="{{route('perfilUsuario', $user->stageName)}}" class="btn btn-outline-primary mx-auto">Ver perfil <i class="fa fa-user-circle ml-2"></i> </a>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </section>   
     @endif
@endsection