@extends('layouts.templateHome')

@section('content')
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
          @if (isset($socialNetwork) && $socialNetwork ?? '') 
            <form id="form" class="form-group" method="POST" action="/home/redesSociales/{{$socialNetwork->slug}}" enctype="multipart/form-data">
                @method('PUT')
                        @csrf
                          <div class="card shadow-lg">
                              <div class="card-header bg-primary p-0">
                                  <div class="text-white text-center py-2">
                                      <h3 class="font-weight-bold"> Actualización de redes sociales</h3>
                                  </div>
                              </div>
                              <div class="card-body p-0">
                                <div class="form-group col-12 col-lg-11 mx-auto">
                                    <label for="icon_id" class="text-dark d-block my-2">Red Social</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                        </div>
                                        <select aria-label="Icono" id="validationIcon" class="custom-select" name="icon_id" required>
                                          <option disabled selected>-- Escoga una red social--</option>
                                          @if (isset($icons) && $icons ?? '')
                                            @foreach ($icons as $i)
                                            <option  value="{{$i->id}}" @if(isset($socialNetwork->icon_id) && $i->id == $socialNetwork->icon_id) selected @endif>
                                                {{$i->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @error('icon_id')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                    @enderror
                                    <label for="link" class="text-dark d-block my-2">Enlace</label>
                                    <div class="input-group mb-2">
                                          <div class="input-group-prepend">
                                              <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-link text-white"></i></div>
                                          </div>
                                        <input aria-label="Enlace" id="validationLink" type="text" name="link" class="form-control" value="{{$socialNetwork->link}}" placeholder="Ingrese el link">
                                    </div>
                                    <p class="text-primary">El campo <b>'Enlace'</b> puede quedar vacío.</p>
                                    <label for="name_profile" class="text-dark d-block my-2">Nombre del perfil o número</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-user text-white"></i></div>
                                        </div>
                                    <input aria-label="Nombre de perfil" id="validationNameProfile" type="text" name="nameProfile" class="form-control" value="{{$socialNetwork->nameProfile}}" placeholder="Ingrese el nombre del perfil o número teléfonico" required>
                                    </div>
                                    @error('nameProfile')
                                        <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                    @enderror
                                    <div class="mx-auto my-4">
                                        <a href="{{route('redesSociales')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                        <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Actualizar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                 </div>
                                </div>       
                              </div>
                          </div>
                      </form>
                @endif
              </div>
        </div>
@endsection


