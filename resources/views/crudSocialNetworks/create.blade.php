@extends('layouts.templateHome')

@section('content')
{{-- @include('common.errors') --}}
<div class="row justify-content-center">
        <div class="col-12 col-lg-12 pb-5">
            <form id="form" class="form-group" method="POST" action="/home/redesSociales" enctype="multipart/form-data">
                    @csrf
                      <div class="card shadow-lg p-0">
                          <div class="card-header bg-primary p-0">
                              <div class="text-white text-center py-2">
                                  <h3 class="font-weight-bold"> Registro de redes sociales</h3>
                              </div>
                          </div>
                          <div class="card-body p-0">
                            <div class="form-group col-12 col-lg-11 mx-auto">
                                <div class="input-group mb-2 my-3 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"> <i class="fa fa-tag text-white"></i> </div>
                                    </div>
                                    <select aria-label="Icono" id="validationIcon" class="custom-select" name="icon_id" required>
                                      <option disabled selected value="">-- Escoga una red social --</option>
                                      @if (isset($icons) && $icons ?? '')
                                        @foreach ($icons as $i)
                                    <option  value="{{$i->id}}" {{old('icon_id') == $i->id ? 'selected': ''}}>{{$i->name}}</option>
                                        @endforeach
                                        </select>
                                      @endif
                                </div>
                                @error('icon_id')
                                    <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                                @enderror
                                <div class="input-group mb-2 my-2 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-link text-white"></i></div>
                                    </div>
                                <input aria-label="Enlace" type="url" id="validationLink" name="link" class="form-control" value="{{old('link')}}" placeholder="Ingrese el link o enlace (Incluya https)">
                            </div>
                            <small class="text-primary">El campo <b>'Enlace'</b> puede quedar vacío.</small>
                                <div class="input-group mb-2 my-3 mx-auto">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text bg-gradient-primary"><i name="icon" class="fa fa-user text-white"></i></div>
                                    </div>
                                <input aria-label="Nombre de perfil" type="text" id="validationNameProfile" name="nameProfile" class="form-control" value="{{old('nameProfile')}}" placeholder="Ingrese el nombre del perfil o número teléfonico" required>
                            </div>
                            @error('nameProfile')
                                <small class="text-danger d-block font-weight-bold my-0">{{$message}} <i class="fa fa-exclamation-triangle"></i> </small>
                            @enderror
                                <div class="mx-auto my-4">
                                    <a href="{{route('redesSociales')}}" class="btn btn-light mx-auto my-1"> <i class="fa fa-arrow-left mr-2"></i>Volver</a>
                                    <button id="btnSubmit" class="btn btn-primary ml-2 mx-auto">Guardar <i class="fa fa-cloud-upload-alt ml-2"></i></button>
                                </div>
                            </div>    
                          </div>
                      </div>
                  </form>
              </div>
@endsection

